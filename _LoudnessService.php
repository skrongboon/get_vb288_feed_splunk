<?php
include_once("Curl.php");
class Audio{
    public $pid;
    public $loudness;
    function __construct($id, $loudness){
        $this->pid = $id;
        $this->loudness = $loudness;
    }
}

class Service {
    public $name;
    public $sid;
    public $audioPids;
    public $audioList;

    function __construct($id, $name, array $audioPids){
        $this->sid = $id;
        $this->name = $name;
        $this->audioPids = $audioPids;
    }
    
}

class LoudnessService {
    private $curlLoudnessResult = null;
    private $loudnessServiceList = null;

    function __construct($url){
        $this->CurlLoudnessData($url);
    }

    public function CurlLoudnessData($url){
        $this->CurlLoudnessData = simplexml_load_string(Curl($url));
        $this->MakeLoudnessToObject();
    }

    private function MakeLoudnessToObject(){
        $this->loudnessServiceList = array();
        foreach($this->CurlLoudnessData->Input[0] as $ethInput){
            foreach($ethInput->Service as $xmlService){
                $service = new Service((string)$xmlService["sid"],(string)$xmlService["name"],explode(",",(string)$xmlService["audiopids"]));
                //findind audio loudness pid
                $audioList = array();
                foreach($ethInput->Pid as $xmlPid){
                    if(in_array((int)$xmlPid["pid"],$service->audioPids)){
                        $arrLoudness = explode(",",(string)$xmlPid->Audio["data"]);
                        $audio = new Audio((int)$xmlPid["pid"],array_sum($arrLoudness)/count($arrLoudness));
                        $audioList[] = $audio;
                    }
                }
                $service->audioList = $audioList;
                $this->loudnessServiceList[] = $service;
            }
        }
    }

    public function GetServiceBySid($sid){
        //found sid in loudnessServiceList return service
        
        return "service"; 
    }
    public function AddLoudnessValue($alarmXml){
        //add loudness value to selected alarm
        echo"<pre>";
        $newAlarmXml = $alarmXml;
        preg_match('/sid=(\d+)/',(string)$alarmXml["trap_text"],$sid);
        if(!count($sid)==0){
            $service = $this->GetServiceBySid($sid[1]);
            foreach(){

            }
        }

        return $newAlarmXml;
    }
}
