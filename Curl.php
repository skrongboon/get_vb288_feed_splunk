<?php
function Curl($url){
	date_default_timezone_set("Asia/Bangkok");

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($ch,CURLOPT_URL,$url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
	curl_setopt($ch, CURLOPT_TIMEOUT, 10);

	$server_output = curl_exec($ch);
	echo "curl get";
	if(!$server_output)
	{
	    echo "Error Number:".curl_errno($ch)."<br>";
	    echo "Error String:".curl_error($ch);
		return false;
	}else{
		echo "curl complete";
		curl_close ($ch);
	    return $server_output;
	}
	//echo "curl complete";
	
	
	
}