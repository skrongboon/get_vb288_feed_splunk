<?php
include_once "Curl.php";
include_once "LoudnessService.php";
if (!empty($argv[1])) {
  parse_str($argv[1], $_GET);
}
date_default_timezone_set("Asia/Bangkok");

$date = date("Y-m-d H:i:s");
$url = $_GET["ip"];
$strUrl288 = "/extractor/alarms";
$strUrl120 = "/probe/alarms";
$strDataPathDTT= "C:/xampp/htdocs/get_vb288/data";
$strDataPathTS= "C:/xampp/htdocs/get_vb288/data-ts";
$ipList = array(
	array("url"=>"172.17.82.46".$strUrl288,"data"=>$strDataPathDTT."/t2.xml")
	,array("url"=>"172.17.82.47".$strUrl288,"data"=>$strDataPathDTT."/cband.xml")
	,array("url"=>"10.10.19.201".$strUrl120,"data"=>$strDataPathTS."/ts01.xml")
	,array("url"=>"10.10.19.202".$strUrl120,"data"=>$strDataPathTS."/ts02.xml")
	,array("url"=>"10.10.19.203".$strUrl120,"data"=>$strDataPathTS."/ts03.xml")
	,array("url"=>"10.10.19.204".$strUrl120,"data"=>$strDataPathTS."/ts04.xml")
	,array("url"=>"10.10.19.205".$strUrl120,"data"=>$strDataPathTS."/ts05.xml")
	,array("url"=>"10.10.19.206".$strUrl120,"data"=>$strDataPathTS."/ts06.xml")
	,array("url"=>"10.10.19.207".$strUrl120,"data"=>$strDataPathTS."/ts07.xml")
	,array("url"=>"10.10.19.208".$strUrl120,"data"=>$strDataPathTS."/ts08.xml")
	,array("url"=>"10.10.19.209".$strUrl120,"data"=>$strDataPathTS."/ts09.xml")
	,array("url"=>"10.10.19.210".$strUrl120,"data"=>$strDataPathTS."/ts10.xml")
	,array("url"=>"10.10.19.220".$strUrl120,"data"=>$strDataPathTS."/ts-cband.xml")
);

$resultCurl = null;
$dataFileExists =false;
$re_install = false;

$dataPath = null;
array_walk($ipList,function($arr)use(&$url,&$dataPath){
	if($arr["url"] == $url){
		$dataPath = $arr["data"];
		echo "FOUND: => $dataPath \n";
		return;
	};
});

if(is_null($dataPath)){
	echo "unknow IP "; 
    return;
}

$fileWrite = $dataPath;

//Get vb288  from endpoint URL
$resultCurl = simplexml_load_string(Curl($url));
if($resultCurl==false){
	echo "\n Curl error Exit!";
	return;
}

// Check old data file exist find lastest alarm id
echo "\r\n check file";
if(file_exists($fileWrite)){
	echo "\r\n Found old file";
	$dataFileExists = true;
	//Get last id from previous file
	$oldData= simplexml_load_file($fileWrite);
	$lastId = (string)$oldData->Alarm[count($oldData->Alarm)-1]["seq"];
	echo "\n Last alarm id: ".$lastId;
	
	//Check install new vb288 case id == seq == 1
	if(($resultCurl->Alarm[0]["seq"] == $resultCurl->Alarm[0]["id"]) && $resultCurl->Alarm[0]["id"] == 1 ){
		echo "\r\n Found VB288 re-install write all alarm";
		$re_install = true;
	}
    
	//if old == new duplicate data
	echo "\r\n new last id".$resultCurl->Alarm[count($resultCurl->Alarm)-1]["seq"];
	if($lastId == (string)$resultCurl->Alarm[count($resultCurl->Alarm)-1]["seq"]){
		echo "\n Duplicate alarm do noting exit!";
		return;	
	}
	//Rename old file
    rename($fileWrite,$fileWrite.".rename");
}

$LoudnessService = new LoudnessService("172.17.82.46/extractor/loudnessdata");
echo "\r\n Write new file";
$writeNewFile = "<AlarmList>".PHP_EOL;
foreach($resultCurl->Alarm as $alarm){
	if($re_install || !$dataFileExists){
		$newAlarm = $LoudnessService->AddLoudnessValue($alarm);
		$writeNewFile .= $alarm->asXML().PHP_EOL;
		//$writeNewFile .= $newAlarm->asXML().PHP_EOL;
		continue;
	}
	if($dataFileExists){
		if($lastId < (string)$alarm["seq"] ){
		    $LoudnessService->AddLoudnessValue($alarm);
			$writeNewFile .= $alarm->asXML().PHP_EOL;
		}
	}
	//else{
	//	$writeNewFile .= $alarm->asXML().PHP_EOL;
	//}
}
$writeNewFile .= "</AlarmList>".PHP_EOL;
//file_put_contents($fileWrite,$writeNewFile);